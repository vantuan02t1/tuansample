import { TuanSampleTemplatePage } from './app.po';

describe('TuanSample App', function() {
  let page: TuanSampleTemplatePage;

  beforeEach(() => {
    page = new TuanSampleTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
