using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace TuanSample.Controllers
{
    public abstract class TuanSampleControllerBase: AbpController
    {
        protected TuanSampleControllerBase()
        {
            LocalizationSourceName = TuanSampleConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
