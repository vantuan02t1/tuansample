﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TuanSample.MultiTenancy.Dto;

namespace TuanSample.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

