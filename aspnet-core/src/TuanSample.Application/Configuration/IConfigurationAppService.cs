﻿using System.Threading.Tasks;
using TuanSample.Configuration.Dto;

namespace TuanSample.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
