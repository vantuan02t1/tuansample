﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using TuanSample.Configuration.Dto;

namespace TuanSample.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : TuanSampleAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
