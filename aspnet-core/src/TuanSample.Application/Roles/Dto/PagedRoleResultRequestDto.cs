﻿using Abp.Application.Services.Dto;

namespace TuanSample.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

