﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TuanSample.Authorization.Accounts.Dto;

namespace TuanSample.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
