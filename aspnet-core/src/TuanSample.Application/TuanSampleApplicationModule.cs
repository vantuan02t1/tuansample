﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using TuanSample.Authorization;

namespace TuanSample
{
    [DependsOn(
        typeof(TuanSampleCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class TuanSampleApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<TuanSampleAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(TuanSampleApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
