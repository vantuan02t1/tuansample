﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TuanSample.Sessions.Dto;

namespace TuanSample.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
