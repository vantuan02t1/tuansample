using System.ComponentModel.DataAnnotations;

namespace TuanSample.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}