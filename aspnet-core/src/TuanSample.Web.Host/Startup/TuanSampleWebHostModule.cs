﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using TuanSample.Configuration;

namespace TuanSample.Web.Host.Startup
{
    [DependsOn(
       typeof(TuanSampleWebCoreModule))]
    public class TuanSampleWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public TuanSampleWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TuanSampleWebHostModule).GetAssembly());
        }
    }
}
