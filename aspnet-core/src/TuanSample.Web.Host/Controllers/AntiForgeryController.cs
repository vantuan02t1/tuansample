using Microsoft.AspNetCore.Antiforgery;
using TuanSample.Controllers;

namespace TuanSample.Web.Host.Controllers
{
    public class AntiForgeryController : TuanSampleControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
