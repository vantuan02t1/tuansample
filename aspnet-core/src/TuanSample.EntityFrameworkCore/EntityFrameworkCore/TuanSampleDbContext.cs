﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using TuanSample.Authorization.Roles;
using TuanSample.Authorization.Users;
using TuanSample.MultiTenancy;

namespace TuanSample.EntityFrameworkCore
{
    public class TuanSampleDbContext : AbpZeroDbContext<Tenant, Role, User, TuanSampleDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public TuanSampleDbContext(DbContextOptions<TuanSampleDbContext> options)
            : base(options)
        {
        }
    }
}
