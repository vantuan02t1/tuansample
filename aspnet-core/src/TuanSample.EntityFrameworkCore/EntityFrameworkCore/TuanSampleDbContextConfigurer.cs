using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace TuanSample.EntityFrameworkCore
{
    public static class TuanSampleDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<TuanSampleDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<TuanSampleDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
