﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using TuanSample.Configuration;
using TuanSample.Web;

namespace TuanSample.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class TuanSampleDbContextFactory : IDesignTimeDbContextFactory<TuanSampleDbContext>
    {
        public TuanSampleDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TuanSampleDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            TuanSampleDbContextConfigurer.Configure(builder, configuration.GetConnectionString(TuanSampleConsts.ConnectionStringName));

            return new TuanSampleDbContext(builder.Options);
        }
    }
}
