﻿using Abp.MultiTenancy;
using TuanSample.Authorization.Users;

namespace TuanSample.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
