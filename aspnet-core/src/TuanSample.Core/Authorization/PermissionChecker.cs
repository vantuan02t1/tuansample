﻿using Abp.Authorization;
using TuanSample.Authorization.Roles;
using TuanSample.Authorization.Users;

namespace TuanSample.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
