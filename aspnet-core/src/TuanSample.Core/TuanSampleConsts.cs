﻿namespace TuanSample
{
    public class TuanSampleConsts
    {
        public const string LocalizationSourceName = "TuanSample";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
